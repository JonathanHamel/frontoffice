package com.directmedia.onlinestore.frontoffice.controller;

import com.directmedia.onlinestore.core.entity.Catalogue;
import com.directmedia.onlinestore.core.entity.ShoppingCart;
import com.directmedia.onlinestore.core.entity.Work;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Optional;

@WebServlet(name = "AddToCartServlet", urlPatterns = {"/addToCart"})
public class AddToCartServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        HttpSession session = req.getSession();
        PrintWriter out = resp.getWriter();
        long idOeuvre = Long.parseLong(req.getParameter("identifiant"));

        ShoppingCart caddie = (ShoppingCart)session.getAttribute("caddie");

        if (caddie == null){
            caddie = new ShoppingCart();
            session.setAttribute("caddie", caddie);
        }

        /*for (Work work: Catalogue.listOfWorks) {
            if (work.getId() == idOeuvre){
                caddie.getItems().add(work);
            }
        }*/

        Optional<Work> optionalWork = Catalogue.listOfWorks.stream().filter(work -> work.getId()==idOeuvre).findAny();
        if (optionalWork.isPresent()){
            caddie.getItems().add(optionalWork.get());
        }

        out.print("<html><body>Oeuvre ajoutée au caddie ("+caddie.getItems().size()+")<br><a href=\"catalogue\">Accèder au catalogue</a></body></html>");

    }
}
