package com.directmedia.onlinestore.frontoffice.controller;

import com.directmedia.onlinestore.core.entity.Catalogue;
import com.directmedia.onlinestore.core.entity.Work;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "WorkDetailsServlet", urlPatterns = {"/work-details"})
public class WorkDetailsServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String idWork = req.getParameter("id");

        for (Work work: Catalogue.listOfWorks) {
            if (work.getId() == Integer.parseInt(idWork)){
                req.setAttribute("id", work.getId());
                req.setAttribute("title", work.getTitle());
                req.setAttribute("genre", work.getGenre());
                req.setAttribute("mainArtist", work.getMainArtist().getName());
                req.setAttribute("release", work.getRelease());
                req.setAttribute("summary", work.getSummary());

                RequestDispatcher rd = req.getRequestDispatcher("/WEB-INF/work-details.jsp");
                rd.forward(req, resp);
            }
        }
    }
}
