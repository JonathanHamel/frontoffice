package com.directmedia.onlinestore.frontoffice.controller;

import com.directmedia.onlinestore.core.entity.Artist;
import com.directmedia.onlinestore.core.entity.Catalogue;
import com.directmedia.onlinestore.core.entity.Work;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;


@WebServlet(name = "CatalogueServlet", urlPatterns = {"/catalogue"})
public class CatalogueServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter out = resp.getWriter();
        if(Catalogue.listOfWorks.isEmpty()){
            Artist damienSaez = new Artist("Damien Saez");
            Artist chrisPratt = new Artist("Chris Pratt");
            Artist ironMaiden = new Artist("Iron Maiden");

            Work workDamienSaez = new Work("J'accuse");
            Work workChrisPratt = new Work("Les gardiens de la galaxie");
            Work workIronMaiden = new Work("Dance of death");

            workDamienSaez.setMainArtist(damienSaez);
            workIronMaiden.setMainArtist(ironMaiden);
            workChrisPratt.setMainArtist(chrisPratt);

            workDamienSaez.setGenre("rock");
            workIronMaiden.setGenre("Heavy metal");
            workChrisPratt.setGenre("super-héros");

            workDamienSaez.setRelease(2004);
            workIronMaiden.setRelease(1996);
            workChrisPratt.setRelease(2008);

            workDamienSaez.setSummary("Un résumé");
            workIronMaiden.setSummary("Un résumé");
            workChrisPratt.setSummary("Un résumé");

            Catalogue.listOfWorks.add(workDamienSaez);
            Catalogue.listOfWorks.add(workIronMaiden);
            Catalogue.listOfWorks.add(workChrisPratt);
        }

        out.print("<html><body><h1>Oeuvres dans le catalogue</h1>");
        for (Work work: Catalogue.listOfWorks) {
            out.print("<a href=\"");
            out.print("work-details?id=" + work.getId()+"\">");
            out.println(work.getTitle()+ " (" + work.getRelease()+ ")");
            out.print("</a>");
            out.print("<br>");
        }

        out.print("</body></html>");

    }
}
